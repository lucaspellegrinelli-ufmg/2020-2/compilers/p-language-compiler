mkdir "build/gen_src"
java -cp lib/JLex-win JLex.Main src/Main.lex
move .\src\Main.lex.java .\build\gen_src\
java -jar lib/java-cup-11b.jar src/Main.cup
move parser.java build/gen_src
move sym.java build/gen_src
javac -Xlint:unchecked -cp lib/java-cup-11b.jar -d build src/semantic/*.java src/nonterminals/*.java src/codegen/*.java build/gen_src/parser.java build/gen_src/sym.java build/gen_src/Main.lex.java
package semantic;

public class TypeError {

  public TypeError (String type, Object operator) {
    System.out.println("Tipo " + type + " incorreto para operador " + operator);
    System.exit(0);
  }

  public TypeError (String type, Object operator, String type2) {
    System.out.println("Impossivel aplicar o operador " + operator + " entre os tipos " + type + " e " + type2);
    System.exit(0);
  }
}
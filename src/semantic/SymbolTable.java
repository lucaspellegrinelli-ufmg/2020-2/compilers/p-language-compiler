package semantic;

import java.util.HashMap;

public class SymbolTable {
  public static HashMap<String, TableEntry> table = new HashMap<String, TableEntry>();

  public static void addToSymbolTable(String id, TableEntry entry) {
    if (!table.containsKey(id)) {
      table.put(id, entry);
    }
  }

  public static TableEntry getIdEntry(String id) {
    return table.get(id);
  }

  public static void printTable() {
    System.out.println(table);
  }
}
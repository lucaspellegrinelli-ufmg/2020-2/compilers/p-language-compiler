package semantic;

public class TableEntry {
  public String identifier;
  public String type;

  public TableEntry (String identifier) {
    this.identifier = identifier;
    this.type = "";
  }

  public TableEntry (String identifier, String type) {
    this.identifier = identifier;
    this.type = type;
  }

  public String getId() {
    return this.identifier;
  }

  public String getType() {
    return this.type;
  }

  @Override
    public String toString() {
        return this.identifier + " - " + this.type;
    }
}
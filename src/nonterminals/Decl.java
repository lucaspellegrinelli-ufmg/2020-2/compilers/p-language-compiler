package nonterminals;

import semantic.SymbolTable;
import semantic.TableEntry;

public class Decl {
  public IdentList identList;
  public String type;

  public Decl(IdentList il, Object t) {
    this.identList = il;
    this.type = (String) t;

    for(int i = 0; i < this.identList.size(); i++) {
      TableEntry entry = new TableEntry(this.identList.get(i), this.type);
      SymbolTable.addToSymbolTable(this.identList.get(i), entry);
    }
  }
}
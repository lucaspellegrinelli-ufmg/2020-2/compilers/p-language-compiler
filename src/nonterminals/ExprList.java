package nonterminals;

import java.util.ArrayList;
import java.util.List;

public class ExprList {
  List<Expr> value = new ArrayList<Expr>();

  public ExprList(Expr co) {
    this.value.add(co);
  }

  public void add(Expr co) {
    this.value.add(co);
  }

  public Expr get(int i) {
    return this.value.get(i);
  }

  public int size() {
    return this.value.size();
  }
}
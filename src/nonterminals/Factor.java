package nonterminals;

import semantic.*;

public class Factor {
  public Object value;
  public String type;

  public Factor(Object f) {
    this.value = f;
    if (this.value instanceof Constant) {
      Constant c = (Constant) this.value;
      this.type = c.type;
    }else if (this.value instanceof Expr){
      Expr c = (Expr) this.value;
      this.type = c.type;    
    }else if (this.value instanceof FunctionRef){
      FunctionRef c = (FunctionRef) this.value;
      this.type = c.type;
    }else if (this.value instanceof Factor){
      Factor c = (Factor) this.value;
      this.type = c.type;
    }else {
      this.type = SymbolTable.getIdEntry((String) this.value).getType();
    }
  }
}
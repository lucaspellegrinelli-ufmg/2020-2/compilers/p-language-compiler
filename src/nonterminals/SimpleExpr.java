package nonterminals;
import semantic.*;

public class SimpleExpr {
  public SimpleExpr simpleExpr;
  public String addop;
  public Term term;
  public String type;

  public SimpleExpr(SimpleExpr se, Object addop, Term te) {
    this.simpleExpr = se;
    this.addop = (String) addop;
    this.term = te;

    //Verificação de tipos

    if (addop == "+" || addop == "-"){
      if (se.type != "Integer" && se.type != "Real" ){
        TypeError n = new TypeError(se.type, addop);
      }
      if (te.type != "Integer" && te.type != "Real" ){
        TypeError n = new TypeError(te.type, addop);
      }

      if (se.type == "Real" || te.type == "Real"){
        this.type = "Real";
      }else{
        this.type = "Integer";
      }
      
    }else if (addop == "or"){
      if (se.type != "Boolean" ){
        TypeError n = new TypeError(se.type, addop);
      }
      if (te.type != "Boolean" ){
        TypeError n = new TypeError(te.type, addop);
      }
      this.type = "Boolean";
    }


  }

  public SimpleExpr(Term te) {
    this.term = te;
    this.type = te.type;
  }

}
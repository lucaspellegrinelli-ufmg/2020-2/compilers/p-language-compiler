package nonterminals;

public class Variable {
  public SimpleVarOrProc simpleVarOrProc;
  public FunctionRefPar functionRefPar;

  public Variable(SimpleVarOrProc svp) {
    this.simpleVarOrProc = svp;
  }

  public Variable(FunctionRefPar frp) {
    this.functionRefPar = frp;
  }
}
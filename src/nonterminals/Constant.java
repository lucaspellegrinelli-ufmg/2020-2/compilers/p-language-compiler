package nonterminals;

public class Constant {
  public Object value;
  public String type;

  public Constant(Object co, String type) {
    this.value = co;
    this.type = type;
  }
}
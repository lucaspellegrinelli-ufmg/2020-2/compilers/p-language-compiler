package nonterminals;

public class LoopStmt {
  public StmtPrefix stmtPrefix;
  public StmtList stmtList;
  public StmtSuffix stmtSuffix;

  public LoopStmt(StmtPrefix sp, StmtList sl, StmtSuffix ss) {
    this.stmtPrefix = sp;
    this.stmtList = sl;
    this.stmtSuffix = ss;
  }

  public LoopStmt(StmtList sl, StmtSuffix ss) {
    this.stmtList = sl;
    this.stmtSuffix = ss;
  }
}
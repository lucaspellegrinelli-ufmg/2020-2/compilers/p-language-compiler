package nonterminals;

import semantic.SymbolTable;
import semantic.TableEntry;

public class AssignStmt{
  public String identifier;
  public Expr expr;

  public AssignStmt(Object id, Expr ex) {
    this.identifier = (String) id;
    this.expr = ex;

    TableEntry varType = SymbolTable.getIdEntry(this.identifier);
    String assignType = this.expr.type;

    if (assignType.length() > 0 && !varType.type.equals(assignType)) {
      System.out.println("Tipo atribuido a variavel '" + this.identifier + "' esta incorreto. (Esperava " + varType.type + " e recebi " + assignType + ")");
    }
  }
}
package nonterminals;

public class Stmt {
  public Label label;
  public UnlabelledStmt unlabelledStmt;

  public Stmt(Label la, UnlabelledStmt ula) {
    this.label = la;
    this.unlabelledStmt = ula;
  }

  public Stmt(UnlabelledStmt ula) {
    this.unlabelledStmt = ula;
  }
}
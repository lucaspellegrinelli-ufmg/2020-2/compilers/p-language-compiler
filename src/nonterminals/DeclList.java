package nonterminals;

import java.util.ArrayList;
import java.util.List;

public class DeclList{
  List<Decl> value;

  public DeclList(Decl co) {
    this.value = new ArrayList<Decl>();
    this.value.add(co);
  }

  public void add(Decl co) {
    this.value.add(co);
  }

  public Decl get(int i) {
    return this.value.get(i);
  }

  public int size() {
    return this.value.size();
  }
}
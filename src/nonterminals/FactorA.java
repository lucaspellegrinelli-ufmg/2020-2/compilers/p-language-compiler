package nonterminals;

public class FactorA {
  public Factor factor;
  public boolean minus;

  public FactorA(Factor f, boolean minus) {
    this.factor = f;
    this.minus = minus;
  }
}
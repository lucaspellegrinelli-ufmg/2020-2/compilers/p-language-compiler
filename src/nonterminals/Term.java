package nonterminals;

import semantic.*;

public class Term {
  public Term term;
  public String mulop;
  public FactorA factorA;
  public String type;

  public Term(FactorA fa) {
    this.factorA = fa;
    this.type = fa.factor.type;
  }

  public Term(Term te, Object mlp, FactorA fa) {
    this.term = te;
    this.mulop = (String) mlp;
    this.factorA = fa;

    //Verificação de tipos
    String te_type = te.factorA.factor.type ;
    String fa_type = fa.factor.type ;

    if (mlp == "*" || mlp == "/" || mlp == "div"){
      if (te_type != "Integer" && te_type != "Real" ){
        TypeError n = new TypeError(te_type, mlp);
      }
      if (fa_type != "Integer" && fa_type != "Real" ){
        TypeError n = new TypeError(fa_type, mlp);
      }

      if (mlp == "*" && te_type == "Integer" && fa_type == "Integer"){
        this.type = "Integer";
      }else{
        this.type = "Real";
      }
      
    }else if (mlp == "mod"){
      if (te_type != "Integer" ){
        TypeError n = new TypeError(te_type, mlp);
      }
      if (fa_type != "Integer" ){
        TypeError n = new TypeError(fa_type, mlp);
      }
      this.type = "Integer";
    }else if (mlp == "and"){
      if (te_type != "Boolean" ){
        TypeError n = new TypeError(te_type, mlp);
      }
      if (fa_type != "Boolean" ){
        TypeError n = new TypeError(fa_type, mlp);
      }
      this.type = "Boolean";
    }


  }

}
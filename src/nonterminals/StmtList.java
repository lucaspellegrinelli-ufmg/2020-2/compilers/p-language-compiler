package nonterminals;

import java.util.ArrayList;
import java.util.List;

public class StmtList {
  List<Stmt> value;

  public StmtList(Stmt co) {
    this.value = new ArrayList<Stmt>();
    this.value.add(co);
  }

  public void add(Stmt co) {
    this.value.add(co);
  }

  public Stmt get(int i) {
    return this.value.get(i);
  }

  public int size() {
    return this.value.size();
  }
}
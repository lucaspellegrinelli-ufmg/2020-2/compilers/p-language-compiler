package nonterminals;

import codegen.*;

public class Program {
  public String identifier;
  public DeclList declList;
  public CompoundStmt compoundStmt;

  public Program(Object id, DeclList dl, CompoundStmt cs) {
    this.identifier = (String) id;
    this.declList = dl;
    this.compoundStmt = cs;

    CodeGenerator.buildIntermediateCode(this);
  }
}
package nonterminals;

public class FunctionRef {
  public FunctionRefPar functionRefPar;
  public String type;

  public FunctionRef(FunctionRefPar frp) {
    this.functionRefPar = frp;
    this.type = "Real";
  }
}
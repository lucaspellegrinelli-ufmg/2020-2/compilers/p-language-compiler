package nonterminals;

public class IfStmt {
  public Cond condition;
  public Stmt statement1;
  public Stmt statement2;

  public IfStmt(Cond co, Stmt st) {
    this.condition = co;
    this.statement1 = st;
  }

  public IfStmt(Cond co, Stmt st, Stmt st2) {
    this.condition = co;
    this.statement1 = st;
    this.statement2 = st2;
  }
}
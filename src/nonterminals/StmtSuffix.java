package nonterminals;

public class StmtSuffix {
  public Cond condition;

  public StmtSuffix() { }

  public StmtSuffix(Cond co) {
    this.condition = co;
  }
}
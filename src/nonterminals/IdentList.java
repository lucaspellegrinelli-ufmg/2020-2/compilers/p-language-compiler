package nonterminals;

import java.util.ArrayList;
import java.util.List;

public class IdentList {
  List<String> value = new ArrayList<String>();

  public IdentList(Object id) {
    this.value.add((String) id);
  }

  public void add(Object id) {
    this.value.add((String) id);
  }

  public String get(int i) {
    return this.value.get(i);
  }

  public int size() {
    return this.value.size();
  }
}
package nonterminals;

public class FunctionRefPar {
  public Variable variable;
  public ExprList exprList;

  public FunctionRefPar(Variable a, ExprList b) {
    this.variable = a;
    this.exprList = b;
  }

  public FunctionRefPar(ExprList a) {
    this.exprList = a;
  }
}
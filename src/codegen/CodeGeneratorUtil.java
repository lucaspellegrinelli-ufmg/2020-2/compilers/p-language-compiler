package codegen;

public class CodeGeneratorUtil {
  public static boolean isOperation(String str) {
    return str.contains("+") || str.contains("-") || str.contains("*") || str.contains("/") ||
           str.contains("<") || str.contains(">") || str.contains("=");
  }

  public static boolean isRelop(String str) {
    return str.contains("=") || str.contains("<") || str.contains(">") || str.contains("<=") ||
           str.contains(">=") || str.contains("!=") || str.contains("not");
  }

  public static boolean isAddop(String str) {
    return str.contains("+") || str.contains("-") || str.contains("or");
  }

  public static boolean isMulop(String str) {
    return str.contains("*") || str.contains("/") || str.contains("div") || str.contains("mod") ||
           str.contains("and");
  }

  public static String inversedOperation(String op) {
    if (op.contains(">=")) return op.replace(">=", "<");
    if (op.contains("<=")) return op.replace("<=", ">");
    if (op.contains("<>")) return op.replace("!=", "=");
    if (op.contains(">")) return op.replace(">", "<=");
    if (op.contains("<")) return op.replace("<", ">=");
    if (op.contains("=")) return op.replace("=", "!=");
    return op;
  }
}
package codegen;

import java.util.List;
import java.util.ArrayList;

import nonterminals.*;
import semantic.*;

public class CodeGenerator {
  public static List<IntermediateCodeLine> intermediateCode = new ArrayList<IntermediateCodeLine>();

  public static void buildIntermediateCode(Program program) { 
    IntermediateCodeLine.setNextCommandLabel(program.identifier);
    buildDeclList(program.declList); 
    buildCompoundStmt(program.compoundStmt);

    if (IntermediateCodeLine.getNextCommandLabel().length() > 0) {
      intermediateCode.add(new IntermediateCodeLine(IntermediateCodeLine.getNextCommandLabel(), ""));
    }

    System.out.println("");
    for(IntermediateCodeLine line : intermediateCode) {
      System.out.print(line.label);
      if (line.label.length() > 0) System.out.print(":");
      System.out.println("\t" + line.code);
    }
    System.out.println("");
  }

  private static void buildDeclList(DeclList dl) {
    for(int i = 0; i < dl.size(); i++) {
      buildDecl(dl.get(i));
    }
  }

  private static void buildDecl(Decl de) {
    for(int i = 0; i < de.identList.size(); i++) {
      SymbolTable.addToSymbolTable(de.identList.get(i), new TableEntry(de.identList.get(i), de.type));
    }
  }

  private static void buildCompoundStmt(CompoundStmt cs) {
    for(int i = 0; i < cs.statementList.size(); i++) {
      buildStmt(cs.statementList.get(i));
    }
  }

  private static void buildStmt(Stmt st) {
    if (st.label == null) {
      buildUnlabelledStmt(st.unlabelledStmt);
    } else {
      IntermediateCodeLine.nextCommandLabel = st.label.identifier;
      buildUnlabelledStmt(st.unlabelledStmt);
    }
  }

  private static void buildUnlabelledStmt(UnlabelledStmt ula) {
    if (ula.value instanceof AssignStmt) {
      buildAssignStmt((AssignStmt) ula.value);
    } else if (ula.value instanceof IfStmt) {
      buildIfStmt((IfStmt) ula.value);
    } else if (ula.value instanceof LoopStmt) {
      buildLoopStmt((LoopStmt) ula.value);
    } else if (ula.value instanceof WriteStmt) {
      buildWriteStmt((WriteStmt) ula.value);
    } else if (ula.value instanceof ReadStmt) {
      buildReadStmt((ReadStmt) ula.value);
    } else if (ula.value instanceof GotoStmt) {
      buildGotoStmt((GotoStmt) ula.value);
    } else if (ula.value instanceof CompoundStmt) {
      buildCompoundStmt((CompoundStmt) ula.value);
    }
  }

  private static void buildAssignStmt(AssignStmt as) {
    List<String> lines = new ArrayList<String>();
    List<String> exprParts = resolveLogicExpr(resolveExpr(as.expr));

    for(int i = 0; i < exprParts.size() - 1; i++) {
      lines.add(exprParts.get(i));
    }

    String lastExpr = exprParts.get(exprParts.size() - 1).split(" = ")[1];
    lines.add(as.identifier + " = " + lastExpr);

    for (String line : lines) {
      intermediateCode.add(new IntermediateCodeLine("", line));
    }
  }

  private static void buildIfStmt(IfStmt is) {
    List<String> lines = new ArrayList<String>();
    List<String> exprParts = resolveLogicExpr(resolveExpr(is.condition.expr));

    for(int i = 0; i < exprParts.size() - 1; i++) {
      lines.add(exprParts.get(i));
    }

    String ifCondition = exprParts.get(exprParts.size() - 1).split(" = ", 2)[1];
    lines.add("if " + CodeGeneratorUtil.inversedOperation(ifCondition) + " goto [somewhere]");

    for (String line : lines) {
      intermediateCode.add(new IntermediateCodeLine("", line));
    }

    int ifGotoIndex = intermediateCode.size() - 1;
    buildStmt(is.statement1);
    
    if(is.statement2 != null) {
      // Cria um goto para pular o código do else
      intermediateCode.add(new IntermediateCodeLine("", "goto [somewhere]"));
      int ifEndGotoIndex = intermediateCode.size() - 1;

      // Coloca o goto do if para pular para 1 linha depois do fim do if
      String outIfLabel = String.valueOf(ifEndGotoIndex + 1);
      intermediateCode.get(ifGotoIndex).refLabel(outIfLabel);
      IntermediateCodeLine.setNextCommandLabel(outIfLabel);
      
      // Escreve o else
      buildStmt(is.statement2);

      // Atualiza o goto do fim do if para apontar para o fim do else
      String outElseLabel = String.valueOf(intermediateCode.size());
      intermediateCode.get(ifEndGotoIndex).refLabel(outElseLabel);
      IntermediateCodeLine.setNextCommandLabel(outElseLabel);
    } else {
      String outIfLabel = String.valueOf(intermediateCode.size());
      intermediateCode.get(ifGotoIndex).refLabel(outIfLabel);
      IntermediateCodeLine.setNextCommandLabel(outIfLabel);
    }
  }

  private static void buildLoopStmt(LoopStmt ls) {
    String loopStartIndex = String.valueOf(intermediateCode.size());
    IntermediateCodeLine.setNextCommandLabel(loopStartIndex);
    loopStartIndex = IntermediateCodeLine.getNextCommandLabel();

    if(ls.stmtPrefix != null) {
      List<String> exprParts = resolveLogicExpr(resolveExpr(ls.stmtPrefix.condition.expr));

      for(int i = 0; i < exprParts.size() - 1; i++) {
        intermediateCode.add(new IntermediateCodeLine("", exprParts.get(i)));
      }

      String ifCondition = exprParts.get(exprParts.size() - 1).split(" = ")[1];
      String ifLine = "if " + CodeGeneratorUtil.inversedOperation(ifCondition) + " goto [somewhere]";
      int loopTestIndex = intermediateCode.size();
      String loopTextLabel = IntermediateCodeLine.getNextCommandLabel().length() > 0 ?
        IntermediateCodeLine.getNextCommandLabel() : String.valueOf(loopTestIndex);
      intermediateCode.add(new IntermediateCodeLine(exprParts.size() == 1 ? loopTextLabel : "", ifLine));

      for(int i = 0; i < ls.stmtList.size(); i++) {
        buildStmt(ls.stmtList.get(i));
      }

      intermediateCode.add(new IntermediateCodeLine("", "goto " + loopStartIndex));
      String outOfLoopLabel = String.valueOf(intermediateCode.size());
      intermediateCode.get(loopTestIndex).refLabel(outOfLoopLabel);
      IntermediateCodeLine.setNextCommandLabel(outOfLoopLabel);
    } else {
      for(int i = 0; i < ls.stmtList.size(); i++) {
        buildStmt(ls.stmtList.get(i));
      }

      if (ls.stmtSuffix.condition != null) {
        List<String> exprParts = resolveLogicExpr(resolveExpr(ls.stmtSuffix.condition.expr));
        for(int i = 0; i < exprParts.size() - 1; i++) {
          intermediateCode.add(new IntermediateCodeLine("", exprParts.get(i)));
        }

        String ifCondition = exprParts.get(exprParts.size() - 1).split(" = ")[1];
        String ifLine = "if " + CodeGeneratorUtil.inversedOperation(ifCondition) + " goto " + loopStartIndex;
        intermediateCode.add(new IntermediateCodeLine("", ifLine));
      }
    }
  }

  private static void buildWriteStmt(WriteStmt ws) {
    for (int i = 0; i < ws.exprList.size(); i++) {
      List<String> exprParts = resolveLogicExpr(resolveExpr(ws.exprList.get(i)));

      for (String line : exprParts) {
        intermediateCode.add(new IntermediateCodeLine("", line));
      }

      String lastVariable = exprParts.get(exprParts.size() - 1).split(" = ")[0];
      intermediateCode.add(new IntermediateCodeLine("", "write " + lastVariable));
    }
  }

  private static void buildReadStmt(ReadStmt rs) {
    for (int i = 0; i < rs.identList.size(); i++) {
      intermediateCode.add(new IntermediateCodeLine("", "read " + rs.identList.get(i)));
    }
  }

  private static void buildGotoStmt(GotoStmt ls) {
    intermediateCode.add(new IntermediateCodeLine("", "goto " + ls.identifier));
  }

  private static List<String> resolveLogicExpr(List<String> exprParts) {
    List<String> subExpr = new ArrayList<String>();

    while (true) {
      int notIndex = -1;
      for(int i = 0; i < exprParts.size(); i++) {
        if (exprParts.get(i).toLowerCase().contains("not ")) {
          notIndex = i;
          break;
        }
      }

      if (notIndex != -1) {
        int tempVarId = subExpr.size();
        subExpr.add("t" + tempVarId + " = " + exprParts.get(notIndex));
        exprParts.set(notIndex, exprParts.get(notIndex).replace("NOT ", "").replace("not ", ""));
      } else {
        break;
      }
    }

    if(exprParts.size() > 1) {
      while(exprParts.size() > 1) {
        int foundIndex = -1;
        for(int i = 0; i < exprParts.size() && foundIndex == -1; i++) {
          if (CodeGeneratorUtil.isMulop(exprParts.get(i))) {
            foundIndex = i;
          }
        }

        for(int i = 0; i < exprParts.size() && foundIndex == -1; i++) {
          if (CodeGeneratorUtil.isAddop(exprParts.get(i))) {
            foundIndex = i;
          }
        }

        for(int i = 0; i < exprParts.size() && foundIndex == -1; i++) {
          if (CodeGeneratorUtil.isRelop(exprParts.get(i))) {
            foundIndex = i;
          }
        }

        int tempVarId = subExpr.size();
        subExpr.add("t" + tempVarId + " = " + exprParts.get(foundIndex - 1) + " " +
                    exprParts.get(foundIndex) + " " + exprParts.get(foundIndex + 1));

        exprParts.set(foundIndex, "t" + tempVarId);
        exprParts.remove(foundIndex + 1);
        exprParts.remove(foundIndex - 1);
      }
    } else {
      subExpr.add("t0 = " + exprParts.get(0));
    }

    return subExpr;
  }

  private static List<String> resolveExpr(Expr ex) {
    List<String> parts = new ArrayList<String>();

    if (ex.simpleExpr2 == null) {
      parts.addAll(resolveSimpleExpr(ex.simpleExpr));
    } else {
      parts.addAll(resolveSimpleExpr(ex.simpleExpr));
      parts.add(ex.relop);
      parts.addAll(resolveSimpleExpr(ex.simpleExpr2));
    }

    return parts;
  }

  private static List<String> resolveSimpleExpr(SimpleExpr se) {
    List<String> parts = new ArrayList<String>();
    
    if (se.simpleExpr == null) {
      parts.addAll(resolveTerm(se.term));
    } else {
      parts.addAll(resolveSimpleExpr(se.simpleExpr));
      parts.add(se.addop);
      parts.addAll(resolveTerm(se.term));
    }

    return parts;
  }

  private static List<String> resolveTerm(Term t) {
    List<String> parts = new ArrayList<String>();

    if(t.term == null) {
      parts.addAll(resolveFactorA(t.factorA));
    } else {
      parts.addAll(resolveTerm(t.term));
      parts.add(t.mulop);
      parts.addAll(resolveFactorA(t.factorA));
    }

    return parts;
  }

  private static List<String> resolveFactorA(FactorA fa) {
    List<String> parts = new ArrayList<String>();

    if (fa.minus) {
      for (String factorPart : resolveFactor(fa.factor)) {
        parts.add("-" + factorPart);
      }
    } else {
      parts.addAll(resolveFactor(fa.factor));
    }

    return parts;
  }

  private static List<String> resolveFactor(Factor f) {
    List<String> parts = new ArrayList<String>();

    if (f.value instanceof String) {
      parts.add((String) f.value); // Identifier
    } else if (f.value instanceof Constant) {
      parts.add(resolveConstant((Constant) f.value));
    } else if (f.value instanceof Expr) {
      parts.addAll(resolveExpr((Expr) f.value));
    } else if (f.value instanceof FunctionRef) {
      parts.addAll(resolveFunctionRefPar(((FunctionRef) f.value).functionRefPar));
    } else if (f.value instanceof Factor) {
      for (String factorPart : resolveFactor((Factor) f.value)) {
        parts.add("NOT " + factorPart);
      }
    }

    return parts;
  }

  private static String resolveConstant(Constant c) {
    if (c.value instanceof Integer) {
      return c.value.toString();
    } else if (c.value instanceof Double) {
      return c.value.toString();
    } else if (c.value instanceof String) {
      int charValue = (c.value.toString()).charAt(1);
      return String.valueOf(charValue);
    } else if(c.value instanceof Boolean) {
      int boolValue = (Boolean.parseBoolean(c.value.toString())) == true ? 1 : 0;
      return String.valueOf(boolValue);
    }

    return "Ununderstood constant";
  }

  private static List<String> resolveFunctionRefPar(FunctionRefPar fr) {
    List<String> parts = new ArrayList<String>();

    if (fr.variable == null) {
      for (int i = 0; i < fr.exprList.size(); i++) {
        parts.addAll(resolveExpr(fr.exprList.get(i)));
      }
    } else {
      parts.addAll(resolveVariable(fr.variable));
      for (int i = 0; i < fr.exprList.size(); i++) {
        parts.addAll(resolveExpr(fr.exprList.get(i)));
      }
    }

    return parts;
  }

  private static List<String> resolveVariable(Variable var) {
    List<String> parts = new ArrayList<String>();

    if (var.simpleVarOrProc == null) {
      parts.addAll(resolveFunctionRefPar(var.functionRefPar));
    } else {
      parts.add(var.simpleVarOrProc.identifier);
      SymbolTable.addToSymbolTable(var.simpleVarOrProc.identifier, new TableEntry(var.simpleVarOrProc.identifier, "Function"));
    }

    return parts;
  }
}
package codegen;

public class IntermediateCodeLine {
  public String label;
  public String code;

  public static String nextCommandLabel = "";

  public IntermediateCodeLine(String label, String code) {
    this.label = label;
    this.code = code;

    if (nextCommandLabel.length() > 0) {
      this.label = nextCommandLabel;
      nextCommandLabel = "";
    }
  }

  public void setLabel(String label) {
    this.label = label;
  }

  public void refLabel(String label) {
    this.code = code.replace("[somewhere]", label);
  }

  public static void setNextCommandLabel(String label) {
    if (nextCommandLabel.length() == 0) {
      nextCommandLabel = label;
    }
  }

  public static String getNextCommandLabel() {
    return nextCommandLabel;
  }
}
mkdir -p "build/gen_src"
java -cp lib/JLex-linux JLex.Main src/Main.lex
mv src/Main.lex.java build/gen_src/
java -jar lib/java-cup-11b.jar src/Main.cup
mv parser.java build/gen_src
mv sym.java build/gen_src
javac -Xlint:unchecked -cp lib/java-cup-11b.jar -d build src/semantic/*.java src/nonterminals/*.java src/codegen/*.java build/gen_src/parser.java build/gen_src/sym.java build/gen_src/Main.lex.java